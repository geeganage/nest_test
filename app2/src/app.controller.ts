import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class AppController {
  @MessagePattern('AddData')
  getHello(data: any): void {
    // tslint:disable-next-line: no-console
    console.log(data);
  }
}
