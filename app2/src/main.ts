import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.REDIS,
    options: {
      url: 'redis://nest_redis:6379',
    },
  });

  // tslint:disable-next-line: no-empty
  app.listen(() => {});
}
bootstrap();
