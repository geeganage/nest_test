import {
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
export class IdData1 {
  @IsString()
  @IsNotEmpty()
  public id1!: string;

  @IsString()
  @IsNotEmpty()
  public id2!: string;
}

// tslint:disable-next-line: max-classes-per-file
export class IdData2 {
  @IsNumber()
  @IsNotEmpty()
  public num1!: number;

  @IsNumber()
  @IsNotEmpty()
  public num2!: number;
}

// tslint:disable-next-line: max-classes-per-file
export class Data {
  @ValidateNested()
  public idData1!: IdData1;

  @ValidateNested()
  public idData2!: IdData2;
}
