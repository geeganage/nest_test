import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { Data } from './data.dto';

@Controller()
export class AppController {
  private client: ClientProxy;

  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.REDIS,
      options: {
        url: 'redis://nest_redis:6379',
      },
    });
  }

  @Post('/push')
  async createHello(@Body(new ValidationPipe()) data: Data): Promise<Data> {
    // tslint:disable-next-line: no-console
    console.log(' data ', data);

    return this.client.emit<Data>('AddData', data).toPromise();
  }
}
