#!/bin/bash
docker-compose build
docker-compose up -d
docker-compose exec nest_test /bin/bash || true
docker-compose stop
docker-compose down
