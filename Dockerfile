FROM node:12.13.1
RUN apt-get update && apt-get install -y sudo && rm -rf /var/lib/apt/lists/*
RUN apt-get install -y git
RUN npm i -g typescript
RUN npm i -g @nestjs/cli
RUN mkdir /home/app
ENV HOME=/home/app
ENV PATH="/home/app/nest/node_modules/.bin:${PATH}"
EXPOSE 4000

WORKDIR $HOME/nest
