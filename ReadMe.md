
## How test
1. execute `./start.sh`
2. run `cd app1/ && yarn && yarn start:dev`

3. execute a second container.
  (Select the none redis image)
    1. run `docker ps | grep nest_test`
    2. run `docker exec -it <container id of docker nest_test image> bash`
    3. run `cd app2/ && yarn && yarn start:dev`

4. The main application is app1.
make post request to `http://localhost/push` 
using below data structure.

```json
{
  "idData1": {
    "id1": "id1",
    "id2": "id2"
  },
  "idData2": {
    "num1": "num1",
    "num2": "num2"
  }
}
```